package com.atlassianlabs.perftools.rest.controllers;

import com.atlassianlabs.perftools.AbstractBaseTest;
import com.atlassianlabs.perftools.services.ProductDiscovererService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class ProductControllerTest extends AbstractBaseTest {
    @MockBean
    private ProductDiscovererService productDiscovererService;

    @Autowired
    private MockMvc mvc;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void doesGetProductByPidReturnFisheye() throws Exception {
        when(productDiscovererService.getProduct(FISH_PID)).thenReturn(Optional.of(fishEyeProduct));

        final String content = mvc.perform(get(String.format("/rest/product/%s", FISH_PID))
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        assertThat("Confluence found and should not be", content, not(containsString(confProductJson)));
        assertThat("JIRA found and should not be", content, not(containsString(jiraProductJson)));
        assertThat("Fecru not found", content, containsString(fishEyeProductJson));
    }

    @Test
    public void doesGetProductByPidReturnEmptyIfNotFound() throws Exception {
        when(productDiscovererService.getProduct(UNKNOWN_PID)).thenReturn(Optional.empty());

        mvc.perform(get(String.format("/rest/product/%s", UNKNOWN_PID))
                .accept(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error").exists());
    }
}