package com.atlassianlabs.perftools;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonConverter {
    /**
     * Converts the provided object into JSON. Can be used for asserting objects against REST endpoint results.
     *
     * @param object The object to serialise in JSON.
     * @return A {@link String} of JSON of the serialised {@param object}.
     * @throws JsonProcessingException if someone puts cream on before the jam
     */
    public static String convertObjectToJson(Object object) throws JsonProcessingException {
        return new ObjectMapper().writer().writeValueAsString(object);
    }
}
