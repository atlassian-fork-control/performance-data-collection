package com.atlassianlabs.perftools.collectors.cpu;

import com.atlassianlabs.perftools.AbstractBaseTest;
import com.atlassianlabs.perftools.helper.RunTimeHelper;
import com.atlassianlabs.perftools.services.FileSystemService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static org.apache.commons.io.IOUtils.toInputStream;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TopCpuUsageTest extends AbstractBaseTest {
    private static final long PID = 4242L;

    @Mock
    private FileSystemService fileSystemService;
    @Mock
    private RunTimeHelper runTimeHelper;
    @Mock
    private Process process;

    @InjectMocks
    private TopCpuUsage topCpuUsage;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        
        final InputStream inputStream = toInputStream(CPU_RESULT_STRING);

        when(runTimeHelper.spawnProcessSafely(any())).thenReturn(Optional.of(process));
        when(process.getInputStream()).thenReturn(inputStream);

        when(fileSystemService.isLinux()).thenReturn(false);
        when(fileSystemService.isSolaris()).thenReturn(false);
    }

    @Test
    public void doesLinuxSpawnProcessAsExpected() throws Exception {
        when(fileSystemService.isLinux()).thenReturn(true);
        final String testResult = topCpuUsage.collectFromProcess(PID);

        verify(runTimeHelper).spawnProcessSafely("bash", "-c", "/usr/bin/top -b -H -n 1 -p " + PID);
        assertThat(testResult, equalTo(successfulCpuUsageResult.getResult()));
    }

    @Test
    public void doesSolarisSpawnProcessAsExpected() throws Exception {
        when(fileSystemService.isSolaris()).thenReturn(true);
        final String testResult = topCpuUsage.collectFromProcess(PID);

        verify(runTimeHelper).spawnProcessSafely("bash", "-c", "/usr/bin/prstat -L -n 500 1 1 -p " + PID);
        assertThat(testResult, equalTo(successfulCpuUsageResult.getResult()));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void doesWindowsThrowException() throws Exception {
        topCpuUsage.collectFromProcess(PID);
    }

    @Test(expected = IOException.class)
    public void ifProcessNullDoWeHandleIOException() throws Exception {
        when(fileSystemService.isLinux()).thenReturn(true);
        when(runTimeHelper.spawnProcessSafely(any())).thenReturn(Optional.empty());

        topCpuUsage.collectFromProcess(PID);
    }
}