package com.atlassianlabs.perftools.collectors.threaddump;

import com.atlassianlabs.perftools.AbstractBaseTest;
import com.atlassianlabs.perftools.virtualmachines.AutoCloseableHotSpotVm;
import com.atlassianlabs.perftools.virtualmachines.VirtualMachineFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.InputStream;

import static org.apache.commons.io.IOUtils.toInputStream;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JStackThreadDumpTest extends AbstractBaseTest {
    private static final String DUMP = "the normie facebook memers demands more cowbell";
    private static final long PID = 1984L;

    @Mock
    private VirtualMachineFactory virtualMachineFactory;
    @Mock
    private AutoCloseableHotSpotVm hotSpotVm;

    @InjectMocks
    private JStackThreadDump jStackThreadDump;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        final InputStream in = toInputStream(DUMP, "UTF-8");

        when(virtualMachineFactory.getHotSpotVirtualMachine(PID)).thenReturn(hotSpotVm);
        when(hotSpotVm.remoteDataDump()).thenReturn(in);
    }

    @Test
    public void doesJStackWorkAsExpected() throws Exception {
        assertThat(jStackThreadDump.collectFromProcess(PID), is(DUMP));
    }

    @Test
    public void doWeDetachVmWhenDone() throws Exception {
        jStackThreadDump.collectFromProcess(PID);

        verify(hotSpotVm).close();
    }

    @Test(expected = NoSuchMethodException.class)
    public void itShouldNotRethrowException() throws Exception {
        when(virtualMachineFactory.getHotSpotVirtualMachine(PID)).thenThrow(new NoSuchMethodException("Mate that's not a method you donkey"));

        jStackThreadDump.collectFromProcess(PID);
    }
}