package com.atlassianlabs.perftools.collectors;

import com.atlassianlabs.perftools.AbstractBaseTest;
import com.atlassianlabs.perftools.collectors.results.ComboResult;
import com.atlassianlabs.perftools.services.ComboService;
import com.atlassianlabs.perftools.services.CpuUsageService;
import com.atlassianlabs.perftools.services.ThreadDumpService;
import com.atlassianlabs.perftools.services.TimeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ComboServiceTest extends AbstractBaseTest {
    private static final long PID = 4242L;

    @Mock
    private CpuUsageService cpuUsageService;
    @Mock
    private ThreadDumpService threadDumpService;
    @Mock
    private TimeService timeService;

    @InjectMocks
    private ComboService service;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        when(cpuUsageService.collectFromPid(PID)).thenReturn(successfulCpuUsageResult);
        when(threadDumpService.collectFromPid(PID)).thenReturn(successfulThreadDumpResult);
    }

    @Test
    public void doesGenerateThreadDumpsAndCpuInfo6Every10SecondsWork() throws Exception {
        assertExecutions(6, 10);
    }

    @Test
    public void doesGenerateThreadDumpsAndCpuInfo30Every1SecondWork() throws Exception {
        assertExecutions(30, 1);
    }

    private void assertExecutions(int totalTimes, int duration) throws InterruptedException, IOException {
        final ComboResult result = service.collectThreadDumpsAndCpuUsage(PID, totalTimes, SECONDS.toMillis(duration));

        verify(timeService, times(totalTimes - 1)).sleep(SECONDS.toMillis(duration));
        verify(cpuUsageService, times(totalTimes)).collectFromPid(PID);
        verify(threadDumpService, times(totalTimes)).collectFromPid(PID);

        assertThat(result.getCpuUsageResults().size(), is(totalTimes));
        assertThat(result.getCpuUsageResults().get(0), is(successfulCpuUsageResult));
        assertThat(result.getThreadDumpResults().size(), is(totalTimes));
        assertThat(result.getThreadDumpResults().get(0), is(successfulThreadDumpResult));
    }
}