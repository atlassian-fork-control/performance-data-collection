@echo off
setlocal
setlocal enabledelayedexpansion

set _PRG_DIR=%~dp0
cd %_PRG_DIR%
set SERVICE_EXE="%_PRG_DIR%\windows-service\service.exe"

rem Check for sudo access, otherwise we cannot uninstall:
net session >nul 2>&1
if errorlevel 1 goto :notAdmin

:checkService
for /f %%i in ('%SERVICE_EXE% status') do set serviceStatus=%%i
if %serviceStatus% == NonExistent goto :serviceDoesNotExist
if %serviceStatus% == Starter goto :stopService
if %serviceStatus% == Stopped goto :uninstallService

:stopService
echo Stopping data-collector service before uninstalling.
%SERVICE_EXE% stop
if errorlevel 1 goto :end

:uninstallService
echo Uninstalling data-collector service.
%SERVICE_EXE% uninstall
if errorlevel 1 goto :end
goto :end

:serviceDoesNotExist
echo The data-collector service does not exist and will not be uninstalled.
goto :end

:notAdmin
echo Unable to uninstall service, as not logged in as an administrator. Right click ^> Run as Administrator to fix this.
goto :end

:end
pause