package com.atlassianlabs.perftools;

import com.atlassianlabs.perftools.converters.ResponseToStringHttpConverter;
import com.atlassianlabs.perftools.converters.ResponseToTxtFileHttpConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ObjectToStringHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.TEXT_PLAIN;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {
    // This lets our REST endpoints provide a response in JSON or TEXT, depending on the Accept header.
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false)
                .favorParameter(false)
                .defaultContentType(APPLICATION_JSON)
                .mediaType("text", TEXT_PLAIN);
    }

    // So we can add our own converters to Spring booty dooty uncle smooty
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new MappingJackson2HttpMessageConverter());
        converters.add(new StringHttpMessageConverter());
        converters.add(new ObjectToStringHttpMessageConverter(new DefaultConversionService()));
        converters.add(new ResponseToStringHttpConverter());
        converters.add(new ResponseToTxtFileHttpConverter());
        super.configureMessageConverters(converters);
    }

    // Needed for swagger-ui to work.
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    // Make it easy to get to the swagger UI.
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", "swagger-ui.html");
    }
}
