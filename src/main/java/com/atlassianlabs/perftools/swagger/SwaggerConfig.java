package com.atlassianlabs.perftools.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static java.util.Collections.singletonList;
import static springfox.documentation.builders.PathSelectors.any;
import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(SWAGGER_2)
                .select()
                .apis(basePackage("com.atlassianlabs.perftools.rest"))
                .paths(any())
                .build()
                .apiInfo(apiInfo())
                .securitySchemes(singletonList(new BasicAuth("basicAuth")));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Performance Data Collector API Documentation")
                .description("Welcome to the API reference. These APIs can be used for collecting performance data. " +
                        "This page documents the REST resources available, along with expected HTTP response codes and sample requests.")
                .version(getClass().getPackage().getImplementationVersion())
                .build();
    }
}
