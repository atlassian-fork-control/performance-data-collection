package com.atlassianlabs.perftools;

import com.atlassianlabs.perftools.services.AnalyticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static java.util.Objects.requireNonNull;

/**
 * Listener that shows where to access the app once it has started.
 */
@Component
public class EventListeners {
    private static final Logger CONSOLE = LoggerFactory.getLogger("console");

    private final Environment environment;
    private final AnalyticsService analyticsService;

    @Autowired
    public EventListeners(@NotNull Environment environment,
                          @NotNull AnalyticsService analyticsService) {
        this.environment = requireNonNull(environment);
        this.analyticsService = requireNonNull(analyticsService);
    }

    @EventListener
    public void onStart(ApplicationReadyEvent event) {
        CONSOLE.info("Performance Data Collector started on http://localhost:{}{}",
                environment.getProperty("local.server.port"),
                environment.getProperty("server.context-path", ""));
        analyticsService.sendAsyncEvent("pdc.server.start");
    }

    @EventListener
    public void onStop(ContextClosedEvent event) {
        CONSOLE.info("Performance Data Collector stopped.");
        analyticsService.sendAsyncEvent("pdc.server.stop");
    }
}
