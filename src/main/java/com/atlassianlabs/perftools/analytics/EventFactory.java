package com.atlassianlabs.perftools.analytics;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.Map;

import static java.lang.System.currentTimeMillis;
import static java.util.Collections.emptyMap;
import static org.apache.commons.lang3.SystemUtils.JAVA_RUNTIME_VERSION;
import static org.apache.commons.lang3.SystemUtils.JAVA_VENDOR;
import static org.apache.commons.lang3.SystemUtils.OS_NAME;

/**
 * Instantiates a {@link Event} with a unique user and server Id generated that is consistent in all events created.
 * The userId is the hashcode of the hostname (if it can be looked up), otherwise it is "Unknown".
 */
@Component
public class EventFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventFactory.class);

    private final String userId;
    private final Map<String, String> userProperties;

    public EventFactory() {
        this.userId = generateUserId();
        this.userProperties = generateUserProperties();
    }

    // We don't use [Amplitude] defaults (such as Country) as this is a bespoke Java library and we don't submit IPs.
    private Map<String, String> generateUserProperties() {
        return ImmutableMap.of(
                "Country", Locale.getDefault().getDisplayCountry(),
                "OS", OS_NAME,
                "Java Version", JAVA_RUNTIME_VERSION,
                "Java Vendor", JAVA_VENDOR
        );
    }

    private String generateUserId() {
        try {
            return Integer.toString(InetAddress.getLocalHost().getHostName().hashCode());
        } catch (UnknownHostException e) {
            LOGGER.warn("Could not lookup hostname to generate deviceId.", e);
            return "Unknown";
        }
    }

    public Event newEvent(String eventType) {
        return newEvent(eventType, emptyMap());
    }

    public Event newEvent(String eventType, Map<String, String> eventProperties) {
        return new Event(userId, eventType, eventProperties, userProperties, currentTimeMillis());
    }
}
