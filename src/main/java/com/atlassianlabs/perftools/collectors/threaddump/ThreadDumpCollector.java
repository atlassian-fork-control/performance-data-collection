package com.atlassianlabs.perftools.collectors.threaddump;

import com.atlassianlabs.perftools.collectors.Collector;

/**
 * Interface is used to indicate collector is specifically for thread dumps.
 */
public interface ThreadDumpCollector extends Collector {
}
