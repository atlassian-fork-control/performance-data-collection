package com.atlassianlabs.perftools.collectors.diskspeed;

import com.atlassianlabs.perftools.collectors.results.DiskSpeedResult;
import com.atlassianlabs.perftools.rest.exceptions.NotFoundException;
import com.atlassianlabs.perftools.services.AnalyticsService;
import com.atlassianlabs.perftools.services.FileSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

import static java.io.File.createTempFile;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

/**
 * Allows for collection information on disk speed. This doesn't implement the normal collector interface as
 * it's the only collector in this category.
 */
@Component
public class DiskSpeedCollector {
    public static final String DISK_SPEED_EVENT_TYPE = "pdc.server.collect.DiskSpeedCollector";
    private final FileSystemService fileSystemService;
    private final AnalyticsService analyticsService;

    @Autowired
    public DiskSpeedCollector(@NotNull final FileSystemService fileSystemService,
                              @NotNull final AnalyticsService analyticsService) {
        this.fileSystemService = requireNonNull(fileSystemService);
        this.analyticsService = requireNonNull(analyticsService);
    }

    /**
     * Runs a Java benchmark on the disk for the provided directory. This tests open, close, read/write and delete.
     * This is NOT the same as disk benchmarks, it tests the execution of Java on the disk which is what
     * we want, not raw disk speed.
     *
     * @param directory The directory to benchmark
     * @param runs      Number of runs to execute
     * @return The results of the test.
     * @throws IOException       If there is any exceptions with file or string parsing operations.
     * @throws NotFoundException If the provided directory is not valid.
     */
    public DiskSpeedResult collectDiskSpeed(final String directory, final int runs) throws IOException, NotFoundException {
        if (!fileSystemService.isValidDirectory(directory)) {
            throw new NotFoundException(format("%s is not a valid directory", directory));
        }

        // Get the tests, run them:
        final File file = createTempFile(directory, "test.txt");
        final List<DiskSpeedOperation> operations = getDiskSpeedOperations(file);
        runOperations(operations, runs);

        final DiskSpeedResult result = new DiskSpeedResult(directory, operations);
        analyticsService.sendAsyncEvent(DISK_SPEED_EVENT_TYPE, result.getAveragesOfOperations());
        return result;
    }

    private void runOperations(final List<DiskSpeedOperation> runners, final int runs) {
        for (int i = 0; i < runs; i++) {
            Object result = null;
            for (final DiskSpeedOperation runner : runners) {
                result = runner.run(result);
            }
        }
    }

    private List<DiskSpeedOperation> getDiskSpeedOperations(final File file) {
        return asList(new DiskSpeedOperation<>("open", (f) -> {
                    try {
                        return new RandomAccessFile(file, "rw");
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                }),
                new DiskSpeedOperation<RandomAccessFile, RandomAccessFile>("r/w", (f) -> {
                    try {
                        f.writeChars("This is a stress test written String\n");
                        f.readLine();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    return f;
                }),
                new DiskSpeedOperation<RandomAccessFile, File>("close", (f) -> {
                    try {
                        f.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    return file;
                }), new DiskSpeedOperation<File, File>("delete", (f) -> {
                    f.delete();
                    return f;
                })
        );
    }
}
