package com.atlassianlabs.perftools.collectors.diskspeed;

import com.google.common.annotations.VisibleForTesting;

import static com.atlassianlabs.perftools.collectors.diskspeed.Util.format;
import static java.lang.Long.compare;

class DiskSpeedTimer implements Comparable {
    private long start;
    private long total;

    DiskSpeedTimer start() {
        start = System.nanoTime();
        return this;
    }

    DiskSpeedTimer stop() {
        total = System.nanoTime() - start;
        return this;
    }

    long getTotal() {
        return total;
    }

    @VisibleForTesting
    void setTotal(long total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return format(total);
    }

    @Override
    public int compareTo(Object o) {
        if (o != null && o.getClass() == this.getClass()) {
            return compare(getTotal(), ((DiskSpeedTimer) o).getTotal());
        } else {
            throw new UnsupportedOperationException("Comparable is only implemented for DiskSpeedTimer.");
        }
    }

    @Override
    public boolean equals(Object obj) {
        return obj == this || obj != null && obj.getClass() == this.getClass() && getTotal() == ((DiskSpeedTimer) obj).getTotal();
    }

    @Override
    public int hashCode() {
        int result = (int) (start ^ (start >>> 32));
        result = 31 * result + (int) (total ^ (total >>> 32));
        return result;
    }
}
