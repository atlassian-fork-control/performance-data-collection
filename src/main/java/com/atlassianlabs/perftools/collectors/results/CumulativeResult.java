package com.atlassianlabs.perftools.collectors.results;

import com.google.common.annotations.VisibleForTesting;

import javax.validation.constraints.NotNull;
import java.util.List;

import static com.google.common.collect.ImmutableList.copyOf;
import static java.lang.String.join;
import static java.util.Objects.requireNonNull;

/**
 * Abstract class that holds multiple failure reasons for any attempted data collections, and the result as a String.
 * It's abstract so we can provide multiple sub-classes for each collection type, and subsequently specific REST API docs
 * on each sub-class.
 */
public abstract class CumulativeResult { //NOSONAR
    @VisibleForTesting
    static final String NO_RESULTS_ERROR = "All attempts to generate a result have failed with the below error(s).";

    private final String generatedBy;
    private final List<String> failureReasons;
    private final String result;

    protected CumulativeResult(@NotNull final List<String> failureReasons, @NotNull final String generatedBy, @NotNull final String result) {
        this.failureReasons = requireNonNull(copyOf(failureReasons));
        this.generatedBy = requireNonNull(generatedBy);
        this.result = requireNonNull(result);
    }

    public String getResult() {
        return result;
    }

    public List<String> getFailureReasons() {
        return failureReasons; //NOSONAR
    }

    public String getGeneratedBy() {
        return generatedBy;
    }

    /**
     * As this result contains failure reasons, what generated it and the actual result converting this into a string is
     * a bit more complicated, than say if it was read into JSON. Due to this we check if results exist and if so return
     * those, otherwise the failure reasons are returned.
     *
     * @return A result if it's present, otherwise the list of the failure reasons.
     */
    @Override
    public String toString() {
        if (getResult().isEmpty()) {
            return NO_RESULTS_ERROR + "\n\n" + join("\n", getFailureReasons());
        } else {
            return getResult();
        }
    }
}
