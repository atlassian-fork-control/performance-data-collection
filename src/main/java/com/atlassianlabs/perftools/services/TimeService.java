package com.atlassianlabs.perftools.services;

import org.springframework.stereotype.Service;

/**
 * Class used to handle delegating time-related functionality so we can mock it out for testing.
 */
@Service
public class TimeService {
    public void sleep(long milliseconds) throws InterruptedException {
        Thread.sleep(milliseconds);
    }
}
