package com.atlassianlabs.perftools.services;

import com.atlassianlabs.perftools.analytics.Event;
import com.atlassianlabs.perftools.analytics.EventFactory;
import com.atlassianlabs.perftools.concurrent.TimeLimitedExecutor;
import com.atlassianlabs.perftools.http.HttpClientFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import static java.lang.Math.toIntExact;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.SECONDS;

@Service
public class AnalyticsService {
    private static final String ANALYTICS_ENDPOINT = "https://api.amplitude.com/httpapi";
    private static final String API_KEY = "d7f700f458452bc002fa7041a613ed63";

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyticsService.class);
    private static final int DEFAULT_CONNECT_TIMEOUT_MS = toIntExact(SECONDS.toMillis(5));

    private final CloseableHttpClient httpClient;
    private final TimeLimitedExecutor timeLimitedExecutor;
    private final EventFactory eventFactory;
    private final ObjectMapper mapper;

    @Autowired
    public AnalyticsService(@NotNull final HttpClientFactory httpClientFactory,
                            @NotNull final TimeLimitedExecutor timeLimitedExecutor,
                            @NotNull final EventFactory eventFactory) {
        this.httpClient = requireNonNull(httpClientFactory).newHttpClient(DEFAULT_CONNECT_TIMEOUT_MS);
        this.timeLimitedExecutor = requireNonNull(timeLimitedExecutor);
        this.eventFactory = requireNonNull(eventFactory);
        this.mapper = new ObjectMapper();
    }

    /**
     * Asynchronously sends an Amplitude event with no additional properties.
     *
     * @param eventType The name of the event.
     */
    public void sendAsyncEvent(String eventType) {
        sendAsyncEvent(eventFactory.newEvent(eventType));
    }

    /**
     * Asynchronously sends an Amplitude event with the provided additional properties.
     *
     * @param eventType       The name of the event.
     * @param eventProperties A map of properties in the format of event_name / event_property to include in the event
     *                        when sent to Amplitude.
     */
    public void sendAsyncEvent(String eventType, Map<String, String> eventProperties) {
        sendAsyncEvent(eventFactory.newEvent(eventType, eventProperties));
    }

    /**
     * Sends an event to Amplitude's analytics API. The API key is set in this class and the Event
     * is serialised into JSON to send with a HttpClient asynchronously.
     *
     * @param event The relevant event to send.
     */
    public void sendAsyncEvent(@NotNull Event event) {
        requireNonNull(event);
        timeLimitedExecutor.runAsync(() -> {
            final HttpPost httpPost = generateRequest(event);

            try (final CloseableHttpResponse response = httpClient.execute(httpPost)) {
                final StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                    LOGGER.debug("Sent event '{}'", event.getEventType());
                } else {
                    LOGGER.debug("Problem sending event '{}', status '{}' reason '{}'.",
                            event.getEventType(),
                            statusLine.getStatusCode(),
                            statusLine.getReasonPhrase());
                }
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                LOGGER.warn("Unable to send analytics event '{}'", event.getEventType(), e);
            }
            return null;
        });
    }

    private HttpPost generateRequest(Event event) throws JsonProcessingException, UnsupportedEncodingException {
        final HttpPost httpPost = new HttpPost(ANALYTICS_ENDPOINT);
        httpPost.addHeader("Content-Type", "application/json");
        httpPost.setEntity(new UrlEncodedFormEntity(asList(
                new BasicNameValuePair("api_key", API_KEY),
                new BasicNameValuePair("event", mapper.writeValueAsString(event))
        )));
        return httpPost;
    }
}
