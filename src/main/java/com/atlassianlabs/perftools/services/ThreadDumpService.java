package com.atlassianlabs.perftools.services;

import com.atlassianlabs.perftools.collectors.results.ThreadDumpResult;
import com.atlassianlabs.perftools.collectors.threaddump.ThreadDumpCollector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
public class ThreadDumpService extends BaseService<ThreadDumpCollector, ThreadDumpResult> {

    @Autowired
    public ThreadDumpService(@NotNull final AnalyticsService analyticsService, @NotNull final ApplicationContext context) {
        super(analyticsService, context, ThreadDumpCollector.class);
    }

    @Override
    protected ThreadDumpResult newResult(@NotNull List<String> failureReasons, @NotNull String generatedBy, @NotNull String result) {
        return new ThreadDumpResult(failureReasons, generatedBy, result);
    }
}
