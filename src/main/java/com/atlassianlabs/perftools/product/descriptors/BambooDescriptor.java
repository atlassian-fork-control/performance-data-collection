package com.atlassianlabs.perftools.product.descriptors;

import org.springframework.stereotype.Component;

@Component
public class BambooDescriptor implements ProductDescriptor {

    @Override
    public String getDisplayName() {
        return "org.apache.catalina.startup.Bootstrap start";
    }

    @Override
    public String getInstProp() {
        return "catalina.home";
    }

    @Override
    public String getHomeProp() {
        return "bamboo.home";
    }

    @Override
    public String getHomeFile() {
        return "atlassian-bamboo/WEB-INF/classes/bamboo-init.properties";
    }

    @Override
    public String getLogPath() {
        return "logs";
    }

    @Override
    public String getName() {
        return "Bamboo";
    }
}
