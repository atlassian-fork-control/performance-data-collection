package com.atlassianlabs.perftools.product.descriptors;

import org.springframework.stereotype.Component;

@Component
public class ConfluenceDescriptor implements ProductDescriptor {

    @Override
    public String getDisplayName() {
        return "org.apache.catalina.startup.Bootstrap start";
    }

    @Override
    public String getInstProp() {
        return "catalina.base";
    }

    @Override
    public String getHomeProp() {
        return "confluence.home";
    }

    @Override
    public String getHomeFile() {
        return "confluence/WEB-INF/classes/confluence-init.properties";
    }

    @Override
    public String getLogPath() {
        return "logs";
    }

    @Override
    public String getName() {
        return "Confluence";
    }
}
