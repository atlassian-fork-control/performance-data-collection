package com.atlassianlabs.perftools.product.descriptors;

import org.springframework.stereotype.Component;

@Component
public class BitbucketTomcatDescriptor implements ProductDescriptor {

    @Override
    public String getDisplayName() {
        return "com.atlassian.stash.internal.catalina.startup.Bootstrap start";
    }

    @Override
    public String getInstProp() {
        return "catalina.base";
    }

    @Override
    public String getHomeProp() {
        return "bitbucket.home";
    }

    @Override
    public String getLogPath() {
        return "log";
    }

    @Override
    public String getName() {
        return "Bitbucket Server";
    }
}
